var personName = '';

async function meeting_button() {
    var meetingButton = document.getElementById('enter_room_button')
    if (meetingButton) {
    meetingButton.click()
    }
}

window.addEventListener("load", function() { 
    var current_url = window.location.href
    if (current_url == 'https://jitsi.catgirl.cloud//' || current_url == 'https://jitsi.catgirl.cloud/'){
    window.flutter_inappwebview.callHandler('callEnded', null);
    } else {
    current_url = document.querySelector("#welcome_page")
    if (current_url) {
        window.flutter_inappwebview.callHandler('callEnded', null);
    } else {
        joinMeeting();
    }
    }
});

function JoiningRoom() {
    var current_url = window.location.href
    if (current_url == 'https://jitsi.catgirl.cloud//' || current_url == 'https://jitsi.catgirl.cloud/') {
    window.flutter_inappwebview.callHandler('callEnded', null);
    } else {
    joinMeeting()
    }
}

let intervalDisposeNotify = setInterval(hidenotify, 800); 

async function hidenotify() {
    try {
    var notifications = document.querySelector("#notifications-container")
    if (notifications) {
        notifications.style.display = 'none';
    }
    } catch(err) {}  
}                   

async function checkRoomConnectivity() {
    try {
    var browserContent = document.querySelector('div[class="css-9hxw86-container"]');
    if (browserContent) {
        var image = browserContent.querySelector('img')
        if (image) {
        image.remove()
        }
        
        var downloadLink = browserContent.querySelector('button[class="css-153cjoe-button-primary-medium-fullWidth"]')
        if (downloadLink) {
        downloadLink.remove()
        }
        
        var downloadLinkLabel = browserContent.querySelector('div[class="css-1ywaxyf-labelDescription"]')
        if (downloadLinkLabel) {
        downloadLinkLabel.style.display = "None"
        }
        
        var downloadAppStoreLinkLabel = browserContent.querySelector('div[class="css-1l3cxcu-linkLabel"]')
        if (downloadAppStoreLinkLabel) {
        downloadAppStoreLinkLabel.style.display = "None"
        }
        
        var orLabel = browserContent.querySelector('div[class="css-1g6mx1q-labelOr"]')
        if (orLabel) {
        orLabel.style.display = "None"
        }
        
        var joinBrowserContent = browserContent.querySelector('div[class="css-11lsl83-supportedBrowserContent"]')
        if (joinBrowserContent) {
        var joinBrowser = joinBrowserContent.querySelector('div[class="css-1l3cxcu-linkLabel"]')
        if (joinBrowser) {
            joinBrowser.click()
        }
        }
    }                
    } catch (err) {
        console.log(err);
    } finally {
    setTimeout(() => {
        var alertnNotify = document.querySelector('.bottom-indicators')
        if (alertnNotify) {
            joinMeeting();    
        }
    }, 4000)
    }
}

async function changeChatEvent(targetElement) {                 
    // Create a new observer instance
    let observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        if (mutation.type === 'childList' && mutation.target === targetElement) {
            joinMeeting()
        }
    });
    });
    
    // Configuration of the observer:
    let config = { attributes: false, childList: true, subtree: false };
    
    // Start observing the target node for configured changes
    observer.observe(targetElement, config);
    
}     

async function changeName() {
    try {
    document.querySelector(".stage-participant-label").firstChild.innerHTML = personName
    }  catch (err) {
    console.log(err);
    }
}

async function joinMeeting() {
    try {
    
    var premeetingContent = document.querySelector('#premeeting-name-input')
    if (premeetingContent) {
    premeetingContent.value = personName
    var premeetingBtnContent = document.querySelector('div[data-testid="prejoin.joinMeeting"]')
    if (premeetingBtnContent) {
        setTimeout(() => {
        premeetingBtnContent.click()
        }, 2000)
    }
    } else {
    var toolbox = document.querySelector(".toolbox-content-items")
    if (toolbox) {
        changeChatEvent(toolbox)
        var allButton = toolbox.querySelectorAll(".toolbox-button")
        if (allButton != null && allButton.length > 0) {
        
        changeName()
        
        try {
        document.querySelector('div[aria-label="Open participants pane"]').parentElement.style.display = 'none';
        } catch (err) {
        console.log(err);
        }
        
        try {
        document.querySelector('div[aria-label="अधिक क्रिया मेनू को टॉगल करें"]').parentElement.style.display = 'none';
        } catch (err) {
        console.log(err);
        try {
            document.querySelector('div[aria-label="More actions"]').parentElement.style.display = 'none';
        } catch (err) {
            console.log(err);
        }
        }
        
        try {
        document.querySelector('div[aria-label="टॉगल स्क्रीनशेयर"]').style.display = 'none';
        } catch (err) {
        console.log(err);
        }
            
        try {
        var endCall = document.querySelector('div[aria-label="Leave the meeting"]');
        endCall.addEventListener('click', function () {
            window.flutter_inappwebview.callHandler('callEnded', null);
        });
        } catch (err) {
        console.log(err);
        }
        
        var image = document.querySelector('a[href="https://jitsi.org"]')
        if (image) {
        image.remove();
        }
        
        var alertnNotify = document.querySelector('.bottom-indicators')
        if (alertnNotify) {
        alertnNotify.remove();   
        }
        
        setTimeout(() => {
        window.flutter_inappwebview.callHandler('callStarted', true);
        }, 800);
        
        }
    } else {
        var premeetingContent = document.querySelector('#premeeting-name-input')
        if (premeetingContent) {
        premeetingContent.value = personName
        var premeetingBtnContent = document.querySelector('div[data-testid="prejoin.joinMeeting"]')
        if (premeetingBtnContent) {
            setTimeout(() => {
            premeetingBtnContent.click()
            }, 2000)
        }
        } else {
        checkRoomConnectivity()
        }
    }
    }
    
    } catch (err) {
    console.log(err);
    }
}
